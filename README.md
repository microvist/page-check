# Page Check

Node scripts to run checks on webpages and report issues.

## Running

### Locally

```
export PAGE_CHECK_START=https://example.com
npm i
npm start
```

### Pipeline

Checks job is ran by schedules or for pipelines created by using Run pipeline
button in the GitLab UI.

## Checks

### Spell Check

Once all pages have been crawled/fetched the spell checker is ran against each
page.

Set words for the spell checker to ignore by adding them to `.spellcheckignore`.

For pipelines populate `.spellcheckignore` by setting a **File** variable named
`SPELL_IGNORE`.
