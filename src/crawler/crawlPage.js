const axios = require('axios')

module.exports = async (uri) => {
  const { data: html } = await axios.get(uri)

  return {
    uri,
    html
  }
}
