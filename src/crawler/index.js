const { URL } = require('url')
const cheerio = require('cheerio')
const crawlPage = require('./crawlPage')

const isInternalLink = (uri) => {
  return uri.startsWith('/') || uri.startsWith('../')
}

// todo - use robots.txt to be more generic
const canCrawlLink = (uri) => {
  return !uri.startsWith('/api') && !uri.startsWith('/random')
}

/**
 * Crawler that attempts to fetch the stating pages content and the content from
 * internal links recursively.
 */
module.exports = async (startPage, maxPages = 100) => {
  console.log(`Crawling ${startPage}`)
  const { pathname, hostname } = new URL(startPage)

  const crawledPages = []
  const crawledUris = []
  const crawlQueue = [pathname]

  while (crawlQueue.length && crawledPages.length < maxPages) {
    const uri = crawlQueue.shift()
    const page = await crawlPage(`https://${hostname}${uri}`)
    crawledPages.push(page)
    crawledUris.push(uri)

    // get page links and add to crawl queue if not already in queue or crawled
    const $ = cheerio.load(page.html)
    $('a').each(function () {
      const uri = $(this).attr('href').split('?')[0]
      if (isInternalLink(uri) && canCrawlLink(uri) && !crawledUris.includes(uri) && !crawlQueue.includes(uri)) {
        crawlQueue.push(uri)
      }
    })
  }

  return crawledPages
}
