const crawler = require('./crawler')
const spellcheck = require('./checks/spellcheck')

// crawl for all possible pages
// map uris to content
// go through map/pages & content and apply checks
const main = async () => {
  console.log(`I will check some pages from ${process.env.PAGE_CHECK_START}!`)
  const pages = await crawler(process.env.PAGE_CHECK_START)

  console.log(pages.map(p => p.uri))
  pages.forEach(page => spellcheck(page))
}

main()
  .then(() => console.log('All done!'))
