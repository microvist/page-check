const fs = require('fs')
const path = require('path')
const Typo = require('typo-js')
const htmlToWords = require('./htmlToWords')

const loadIgnoreFile = () => {
  const ignoreFile = path.resolve(__dirname, '../../../.spellcheckignore')

  if (fs.existsSync(ignoreFile)) {
    const data = fs.readFileSync(ignoreFile, { encoding: 'utf8', flag: 'r' })
    return data.split('\n')
  }
  return []
}

const ignoreSpelling = loadIgnoreFile()

const shouldCheck = (word) => {
  if (ignoreSpelling.includes(word)) {
    return false
  }
  return true
}

module.exports = (page) => {
  const { html, uri } = page

  const dictionary = new Typo('en_GB', null, null, {
    dictionaryPath: __dirname,
    asyncLoad: false
  })

  const words = htmlToWords(html)
  words.forEach(word => {
    if (shouldCheck(word)) {
      const isSpelledCorrectly = dictionary.check(word)
      if (!isSpelledCorrectly) {
        console.log(uri, word)
      }
    }
  })
}
