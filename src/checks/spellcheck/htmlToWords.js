const cheerio = require('cheerio')

module.exports = (html) => {
  // The HTML may be minified so lets introduce some extra spaces so words are
  // split properly
  const spacedHtml = html
    .replace(/<\//g, ' </')
    .replace(/\/>/g, ' /> ')

  const emailTest = /(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g
  const websiteTest = /(https?:\/\/)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)/g

  const formatted = cheerio.load(spacedHtml).text()
    .replace(/(etc\.|e\.g\.|i\.e\.)/g, '') // get rid common abbreviations
    .replace(emailTest, '') // remove email addresses
    .replace(websiteTest, '') // remove website addresses
    .replace(/[£$.?!,"&():;/><[\]]/g, ' ') // get rid of punctuation
    .replace(/\S*\d+\S*/g, '') // get rid of words with numbers
    .replace(/\s([A-Z][A-Z]+s?)\s/g, ' ') // remove acronyms
    .replace(/\|/g, ' ') // get rid of pipes
    .replace(/\s-\s/g, ' ') // remove "-" not used to join words
    .replace(/\s\s+/g, ' ') // replace any or multiple whitespace with single space
    .trim() // remove leading and trailing space

  return formatted.split(' ')
}
