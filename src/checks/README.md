# Checks

Checks should be functions that accept a URI and HTML content as arguments. They
assume the page content already crawled and passed as HTML in the second
argument. A third argument of options specific to the check may also be passed.

Each check is expected to output an array of results. Results should have a
title and description.

```
const uri = '/'
const html = '<p>Hello Wold</p>'

const results = spellcheck(uri, html)

// [{
//   title: 'spelling error',
//   description: 'Wold'
// }]
```
