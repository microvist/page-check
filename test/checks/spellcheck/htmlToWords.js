const { assert } = require('chai')
const htmlToWords = require('../../../src/checks/spellcheck/htmlToWords')

describe('htmlToWords', () => {
  it('split words where html tags have no whitespace', () => {
    const html = '<ul><li>foo</li><li>bar</li></ul>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar'], 'words split by tags')
  })
  it('strip sentence periods', () => {
    const html = '<p>foo bar baz.</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'baz'], 'words split and period ignored')
  })
  it('strip sentence exclamation marks', () => {
    const html = '<p>foo bar baz!</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'baz'], 'words split and exclamation mark ignored')
  })
  it('strip sentence question marks', () => {
    const html = '<p>foo bar baz?</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'baz'], 'words split and question mark ignored')
  })
  it('strip words with numbers', () => {
    const html = '<p>an increase in 44% was found</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['an', 'increase', 'in', 'was', 'found'], 'words with numbers ignored')
  })
  it('strips e.g. abbreviations', () => {
    const html = '<p>e.g. foo bar challenge</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'challenge'], 'e.g. abbreviations ignored')
  })
  it('strips i.e. abbreviations', () => {
    const html = '<p>i.e. foo bar challenge</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'challenge'], 'i.e. abbreviations ignored')
  })
  it('strips etc. abbreviations', () => {
    const html = '<p>foo bar challenge etc.</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'challenge'], 'etc. abbreviations ignored')
  })
  it('strips commas', () => {
    const html = '<p>foo, bar and baz</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'and', 'baz'], 'commas are removed')
  })
  it('strips double quotes', () => {
    const html = '<p>foo "bar" baz</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'baz'], 'quotes are removed')
  })
  it('strips ampersand symbols', () => {
    const html = '<p>foo & bar</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar'], 'ampersands are removed')
  })
  it('strips pipes', () => {
    const html = '<p>foo | bar | baz</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'baz'], 'pipes are removed')
  })
  it('strips dashes not connecting words', () => {
    const html = '<p>foo-bar is a baz - qux</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo-bar', 'is', 'a', 'baz', 'qux'], 'dashes not connecting words are removed')
  })
  it('can deal with multiple spaces between words', () => {
    const html = '<p>foo  bar    baz</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'baz'], 'words split even with multiple spaces')
  })
  it('can deal with different whitespace between words', () => {
    const html = '<p>foo  bar  baz</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'baz'], 'words split even with spaces and tabs between')
  })
  it('strips brackets', () => {
    const html = '<p>(foo bar baz)</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'baz'], 'brackets are removed')
  })
  it('strips colons', () => {
    const html = '<p>foo: bar</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar'], 'colons are removed')
  })
  it('strips semi colons', () => {
    const html = '<p>foo; bar</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar'], 'semi colons are removed')
  })
  it('replaces forward slashes with a space', () => {
    const html = '<p>foo/bar/baz</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'baz'], 'forward slashes seen as word separators')
  })
  it('strips email addresses', () => {
    const html = '<p>foo bar@example.com baz</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'baz'], 'emails are not considered a word')
  })
  it('strips greater than symbols', () => {
    const html = '<p>foo &gt; bar</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar'], 'greater than symbol ignored')
  })
  it('strips less than symbols', () => {
    const html = '<p>foo &lt; bar</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar'], 'less than symbol ignored')
  })
  it('ignores acronyms', () => {
    const html = '<p>foo BAR Baz</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'Baz'], 'acronyms ignored')
  })
  it('ignores acronyms surrounded by punctuation', () => {
    const html = '<p>foo (BAR) Baz</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'Baz'], 'acronyms ignored')
  })
  it('ignores pluralized acronyms', () => {
    const html = '<p>foo BARs Baz</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'Baz'], 'pluralized acronyms ignored')
  })
  it('strips website addresses', () => {
    const html = '<p>foo www.ico.org.uk bar.</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar'], 'website ignored')
  })
  it('strips website addresses with https protocol', () => {
    const html = '<p>foo https://www.ico.org.uk bar.</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar'], 'website ignored')
  })
  it('strips website addresses with http protocol', () => {
    const html = '<p>foo http://www.ico.org.uk bar.</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar'], 'website ignored')
  })
  it('strips currency symbols', () => {
    const html = '<p>foo £ bar $ baz $3.50 £2,99 €4-20.</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'baz'], 'currency stripped')
  })
  it('strips square bracket symbols', () => {
    const html = '<p>foo [bar] ] [ baz</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar', 'baz'], 'square brackets stripped')
  })
  it('replaces self closing tags with spaces', () => {
    const html = '<p>foo<br />bar</p>'
    const words = htmlToWords(html)
    assert.sameMembers(words, ['foo', 'bar'], 'br tag separates words')
  })
})
